# PCIe Example for ZPFC board

This example based on Xilinx Answer 65444. Refer https://www.xilinx.com/Attachment/Xilinx_Answer_65444_Linux.pdf for further information.
Video description can be found at https://www.youtube.com/watch?v=TzzzM97L4HI and https://www.youtube.com/watch?v=WcEvAvtXL94

## Vivado project

Vivado project can be found in "vivado" folder. The project was made for Vivado 2018.3.

## Linux Driver

Linux driver build example for Ubuntu 18.04 LTS.

### General notes
To use a custom (unsigned) driver with new Linux systems you need to disable "Secure Boot" option in BIOS. 
It prevents "permission failed" errors while insmod/modprobe commands.

### Prepare Linux system

- Install packages used for kernel build
```shell
sudo apt-get install build-essential checkinstall
sudo apt-get install linux-headers-$(uname -r)
```

- Download Xilinx_Answer_65444_Linux_Files_rel20180420.zip from https://www.xilinx.com/support/answers/65444.html

- Unzip archive
```shell
$Linux> unzip Xilinx_Answer_65444_Linux_Files_rel20180420.zip
```

- Build driver
```shell
$Linux> cd Xilinx_Answer_65444_Linux_Files_rel20180420/xdma/
$Linux> make
```

- Build tests
```shell
$Linux> cd ../tools
$Linux> make
```

### Running the Application

- Load driver
```shell
$Linux> cd ../tests
$Linux> chmod +x *.sh
$Linux> sudo ./load_driver.sh
```

- Run tests
```shell
$Linux> sudo ./run_test.sh
```

- Registers acess

Read/Write AXI GPIO core connected to DIP Switches and PL LEDS
```shell
$Linux> cd ../tools
$Linux> #Set LEDS
$Linux> sudo ./reg_rw /dev/xdma0_user 0x00000000 32 1
$Linux> #Read DIP Switches
$Linux> sudo ./reg_rw /dev/xdma0_user 0x00000000
```

## Related documents

- https://www.xilinx.com/Attachment/Xilinx_Answer_65444_Linux.pdf
- https://www.xilinx.com/Attachment/Xilinx_Answer_71435_XDMA_Debug_Guide.pdf
- https://www.youtube.com/watch?v=TzzzM97L4HI
- https://www.youtube.com/watch?v=WcEvAvtXL94
- https://www.xilinx.com/support/documentation/ip_documentation/xdma/v4_1/pg195-pcie-dma.pdf
