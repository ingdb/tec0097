# Vivado project for ZPFC board

## General description
This project is showing basic PCIe communications.

## Files

* create_project.sh - Project creation shell file.
* create_project.bat - Project creation batch file.
* create_project.tcl - Vivado tcl script to restore project.
* open_project.sh - Open existing project shell file.
* create_project.bat - Open existing project batch file.
* create_project.tcl - Vivado tcl script to open project.
* project_1/TOP.tcl - Saved project block design.
* ip_lib - Folder to store user IPs.
* constraints - Folder to store user constraints.

## Create project

Run create_project.bat or create_project.sh for Linux systems.

## Save project

* In Vivado GUI run "File"->"Export"->"Export Block Design..."
* Navigate to project home folder.
* Replace existing TOP.tcl file by new one.

## Build software

In Vivado
1. File -> Export -> Export Hardware...
2. Click "OK"
3. File -> Launch SDK 
4. Click "OK"

In SDK
1. File -> New -> Application Project...
2. Set "Project name" as "FSBL"
3. Click "Next >"
4. Select "Zynq FSBL" from "Available Templates"
5. Click "Finish"
6. Select FSBL->src folder in "Project Explorer" tab
7. Select "Import..." from right-click menu
8. Select General->File System 
9. Click "Next >"
10. Click "Browse..."
11. Navigate to "sw" folder
12. Click "OK"
13. Select fsbl_hooks.h 
14. Click "Finish"
15. Click "Yes"
16. Wait projet to rebuild
