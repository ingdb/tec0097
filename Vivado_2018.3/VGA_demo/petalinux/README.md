# Petalinux project

## Build steps

- Activate Petalinux environment (replace "/path/to/petalinux/install" by your real path to petalinux)
```shell
source /path/to/petalinux/install/settings.sh
```

- Copy HDF (Hardware Description File) from SDK to Petalinux folder or use prebuilt one.

- Import HDF to the project
```shell
petalinux-config --get-hw-description
```

- Build Petalinux
```shell
petalinux-build
```

- Copy images/linux/u-boot.elf to integrate to BOOT.bin

- Build BOOT.bin in SDK using FSBL.elf bitstream and u-boot.elf

- Copy BOOT.bin to SD card

- Copy images/linux/image.ub to SD card
