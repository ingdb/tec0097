repo for the TEC0097 ZYNQ PCIe FMC carrier board

maintained by
Dübon Engineering GmbH
Teichäcker 4, D-72127 Tübingen-Kusterdingen
Registergericht: Amtsgericht Stuttgart, HRB 760981
Geschäftsführer: Matthias Dübon
www.duebon-engineering.de

clone repo with
git clone https://ingdb@bitbucket.org/ingdb/tec0097.git